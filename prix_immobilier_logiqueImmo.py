# -*- coding: utf-8 -*-
import requests
import requests_cache
import pandas as pd
import time
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
#Pour accepter les '€'
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import json
import plotly.express as px

requests_cache.install_cache("bases_scraping", expire_after=10e5)


url = "https://www.logic-immo.com/vente-immobilier-paris-75,100_1/options/groupprptypesids=1,2/page="

headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'
    }

allPrices = []

#Return les NB premiers liens
def getPages(token, nb):
    pages = []
    for i in range (1,nb):
        j= token+str(i)
        # print(j)
        pages.append(j)
    return pages

#Nettoie le prix (espaces et signes €)
def clean_price(price):
    price = price.replace("€","")
    price = price.replace(" ","")
    return int(price)

def clean_arrondissement(adresse):
    start = adresse.find('(')
    end = adresse.find(')')
    # print(adresse[start-1:end])
    return adresse[start+1:end]

def number_rooms(numberRoom):
    return 'F'+str(numberRoom)

def convertStringToFloat(sup):
    try:
        sup = float(sup)
    except:
        sup = 0.0
    return sup

def getInfo(tab):
    print(tab)
    for i in tab:
        res = requests.get(i, timeout=10)
        soup = BeautifulSoup(res.content,'lxml')
        table = soup.find_all('div', {"class": "offer-details"})
        time.sleep(1)
        for j in table:
            details = []
            #Recup du prix
            try:
                price = j.find('span').getText()
                price = clean_price(price)
            except:
                price = 0.0

            try: 
                #recup de la superficie
                superficie = j.find('span', {"class": "offer-area-number"}).getText()
            except:
                superficie = 0

            try: 
                #Nb de piece qu'il contient
                nbPiece = j.find('span', {"class": "offer-rooms-number"}).getText()
                nbPiece = number_rooms(nbPiece)
            except:
                nbPiece = 0

            try :
                arrondissement = j.find('span', {"class": "offer-details-location--locality"}).getText()
                arrondissement=clean_arrondissement(arrondissement)
                print(arrondissement)
            except:
                 print("pas d'arrondissement")
                 arrondissement = 'Autre'
            #Nettoyage prix
            print(price)
            print(convertStringToFloat(superficie))
            print(nbPiece)
            
            details.append(str(arrondissement))
            details.append(price)
            details.append(nbPiece)
            details.append(convertStringToFloat(superficie))

            allPrices.append(details)




pages = getPages(url,232)
getInfo(pages)
# print(allPrices)

biensImmobDF = pd.DataFrame(allPrices, columns = ['Arrondissement','Prix','NombrePieces','Superficie'])
biensImmobDF = biensImmobDF.drop(biensImmobDF[biensImmobDF.Arrondissement == '7501'].index)
biensImmobDF = biensImmobDF.drop(biensImmobDF[biensImmobDF.Superficie == '0.0'].index)

print(biensImmobDF)

biensImmobDF.to_csv(r'/Users/adamebenadjal/Downloads/Adame.csv', index = None, header=True)


#Ecart type par arrondissement
biensImmobDF.groupby('Arrondissement').std().to_csv(r'/Users/adamebenadjal/Downloads/ecartType.csv', index = None, header=True)

#Moyenne par arrondissement
biensImmobDF.groupby('Arrondissement').mean().to_csv(r'/Users/adamebenadjal/Downloads/Moyenne.csv', index = None, header=True)

# Nombre d'annonces par arrondissement
nbAnnonces_Arrondissement = biensImmobDF.groupby('Arrondissement').agg(len)
print(nbAnnonces_Arrondissement)

nbAnnonces_Arrondissement['Arrondissement'] = nbAnnonces_Arrondissement.index.values
type(nbAnnonces_Arrondissement.index.values)

nbAnnonces_Arrondissement['Arrondissement'] = nbAnnonces_Arrondissement['Arrondissement'].apply(str)
nbAnnonces_Arrondissement['Arrondissement'] = nbAnnonces_Arrondissement['Arrondissement']+'p'
print(nbAnnonces_Arrondissement)
print(nbAnnonces_Arrondissement.dtypes)
fig = px.bar(nbAnnonces_Arrondissement, x='Arrondissement', y='Prix')
fig.show()


# Nombre d'annonces normalisées par arrondissement
nbAnnonces_norm_Arrondissement = biensImmobDF.groupby('Arrondissement').agg(len)
nbAnnonces_norm_Arrondissement['Superficie_Arrondissement'] =  [1.83,1,1.17,1.6,2.54,2.15,4.09,3.88,2.18,2.89,3.67,6.37,7.15,5.64,8.48,7.91,5.67,6.01,6.79,5.98,7.85, 1] 
nbAnnonces_norm_Arrondissement['NB_Normalise'] = nbAnnonces_norm_Arrondissement['Prix'] / nbAnnonces_norm_Arrondissement['Superficie_Arrondissement']  
print("$$$$$$$$$")
print(nbAnnonces_norm_Arrondissement)

nbAnnonces_norm_Arrondissement['Arrondissement'] = nbAnnonces_norm_Arrondissement.index.values
type(nbAnnonces_Arrondissement.index.values)

nbAnnonces_norm_Arrondissement['Arrondissement'] = nbAnnonces_norm_Arrondissement['Arrondissement'].apply(str)
nbAnnonces_norm_Arrondissement['Arrondissement'] = nbAnnonces_norm_Arrondissement['Arrondissement']+'p'
print(nbAnnonces_norm_Arrondissement)
print(nbAnnonces_norm_Arrondissement.dtypes)
fig = px.bar(nbAnnonces_norm_Arrondissement, x='Arrondissement', y='NB_Normalise')
fig.show()

# Moyenne de prix par arrondissement
moyenne_Arrondissement = biensImmobDF.groupby('Arrondissement').mean()
print("===========")
print(moyenne_Arrondissement)

moyenne_Arrondissement['Arrondissement'] = moyenne_Arrondissement.index.values
type(moyenne_Arrondissement.index.values)

moyenne_Arrondissement['Arrondissement'] = moyenne_Arrondissement['Arrondissement'].apply(str)
moyenne_Arrondissement['Arrondissement'] = moyenne_Arrondissement['Arrondissement']+'p'
print(moyenne_Arrondissement)
print(moyenne_Arrondissement.dtypes)
fig = px.bar(moyenne_Arrondissement, x='Arrondissement', y='Prix')
fig.show()


print(biensImmobDF.groupby('Arrondissement').std())
print(biensImmobDF.groupby('Arrondissement').mean())


# Prix moyen au m2 par arrondissement
moyenne_Arrondissement = biensImmobDF.groupby('Arrondissement').mean()
print("===========")
print(moyenne_Arrondissement)

moyenne_Arrondissement['Arrondissement'] = moyenne_Arrondissement.index.values
type(moyenne_Arrondissement.index.values)

moyenne_Arrondissement['Arrondissement'] = moyenne_Arrondissement['Arrondissement'].apply(str)
moyenne_Arrondissement['Arrondissement'] = moyenne_Arrondissement['Arrondissement']+'p'
moyenne_Arrondissement['Prix_m2'] = moyenne_Arrondissement['Prix'] / moyenne_Arrondissement['Superficie']
print(moyenne_Arrondissement)
print(moyenne_Arrondissement.dtypes)
fig = px.bar(moyenne_Arrondissement, x='Arrondissement', y='Prix_m2')
fig.show()


print(biensImmobDF.groupby('Arrondissement').std())
print(biensImmobDF.groupby('Arrondissement').mean())

# Nuage prix_m2 par surface
biensImmobDF['Prix_m2'] = biensImmobDF['Prix'] / biensImmobDF['Superficie']
fig = px.scatter(biensImmobDF, x="Superficie", y="Prix_m2")
fig.show()



# Boxplot (dispersion) des prix par arrondissement
biensImmobDF['Arrondissement'] = biensImmobDF['Arrondissement'].apply(str)
biensImmobDF['Arrondissement'] = biensImmobDF['Arrondissement']+'p'
fig = px.box(biensImmobDF, x="Arrondissement", y="Prix")
fig.show()

